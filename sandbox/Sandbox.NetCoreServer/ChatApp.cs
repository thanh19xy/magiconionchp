using System.Collections.Generic;
using System.Threading.Tasks;
using MagicOnion;
using MagicOnion.Server.Hubs;
using MessagePack;

namespace Sandbox.NetCoreServer
{
    public interface IChatHubReceiver
    {
        void OnJoin(string name);

        void OnLeave(string name);

        void OnSendMessage(MessageResponse message);
    }

    public interface IChatHub : IStreamingHub<IChatHub, IChatHubReceiver>
    {
        Task JoinAsync(JoinRequest request);

        Task LeaveAsync();

        Task SendMessageAsync(string message);

        // It is not called because it is a method as a sample of arguments.
        Task SampleMethod(List<int> sampleList, Dictionary<int, string> sampleDictionary);
    }

    /// <summary>
    /// Message information
    /// </summary>
    [MessagePackObject]
    public struct MessageResponse
    {
        [Key(0)] public string UserName { get; set; }

        [Key(1)] public string Message { get; set; }
    }

    /// <summary>
    /// Room participation information
    /// </summary>
    [MessagePackObject]
    public struct JoinRequest
    {
        [Key(0)] public string RoomName { get; set; }

        [Key(1)] public string UserName { get; set; }
    }

    public class ChatApp : StreamingHubBase<IChatHub, IChatHubReceiver>, IChatHub
    {
        private string UserName { get; set; }

        private IGroup Room { get; set; }

        public async Task JoinAsync(JoinRequest request)
        {
            UserName = request.UserName;
            Room = await Group.AddAsync("InMemoryRoom:" + request.RoomName);
        }

        public async Task LeaveAsync()
        {
            BroadcastExceptSelf(Room).OnSendMessage(new MessageResponse()
                {UserName = this.UserName, Message = "SYSTEM_MESSAGE_LEAVE_USER"});
            await Room.RemoveAsync(this.Context);
        }

        public Task SendMessageAsync(string message)
        {
            // broadcast to connected group(same roomname members).
            if (Room != null)
            {
                Broadcast(Room).OnSendMessage(new MessageResponse() {UserName = this.UserName, Message = message});
            }

            return Task.CompletedTask;
        }

        public Task SampleMethod(List<int> sampleList, Dictionary<int, string> sampleDictionary)
        {
            throw new System.NotImplementedException();
        }
    }
}