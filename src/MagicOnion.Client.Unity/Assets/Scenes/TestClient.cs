﻿using System;
using Grpc.Core;
using MagicOnion.Client;
using Sandbox.NetCoreServer.Services;
using UnityEngine;

public class TestClient : MonoBehaviour
{
    // Start is called before the first frame update
    private async void Start()
    {
        // standard gRPC channel
        var channel = new Channel("localhost", 12345, ChannelCredentials.Insecure);

// get MagicOnion dynamic client proxy
        var client = MagicOnionClient.Create<IMyFirstService>(channel);

// call method.
        var result = await client.SumAsync(100, 200);
        Debug.Log("Client Received:" + result);
    }
}